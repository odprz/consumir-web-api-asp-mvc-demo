﻿// Timer para controlar estado del proceso
var myTimer;
//Manejador de evento para boton de inicio
function OnButtonClick(s, e, startUrl, progressUrl) {
    StartActionOnServer(startUrl);
    StartTimer(progressUrl);
}
// Funcion que permite inicializar el proceso en el servidor
function StartActionOnServer(startUrl) {
    $.ajax({
        type: 'POST',
        url: startUrl,
        dataType: 'json'
    }).done(ProgressCallbackComplete).fail(CallbackError);
}

// Completa correctamente el proceso
function ProgressCallbackComplete(data) {
    StopTimer();
    alert('La migración de bienes ha sido completada con éxito');
    alert(`Se recuperaron ${data.ElementsMigrated} elementos en ${data.ElapsedTime}`);

}

// Inicializa la verificacion del proceso y se mantiene observando el progreso  
function StartTimer(progressUrl) {
    btnStart.SetEnabled(false);
    myProgressBar.SetVisible(true);
    myTimer = setInterval(function () {
        $.ajax({
            type: 'POST',
            url: progressUrl,
            dataType: 'json'
        }).done(TimerCallbackComplete).fail(CallbackError);
    }, 1000);
}
function StopTimer() {
    myProgressBar.SetVisible(false);
    myProgressBar.SetPosition(0);
    btnStart.SetEnabled(true);
    if (myTimer) {
        clearInterval(myTimer);
        myTimer = null;
    }
}
function TimerCallbackComplete(data) {
    myProgressBar.SetPosition(parseInt(data.ProgressResult));
}
// Si existe algun error en el Callback
function CallbackError() {
    StopTimer();
    alert('Sucedio un error inesperado al intentar migrar los datos. Err:x001-cll-bck');
}