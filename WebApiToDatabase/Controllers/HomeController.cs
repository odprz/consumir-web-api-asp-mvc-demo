﻿using DevExpress.Web;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApiToDatabase.Data;

namespace WebApiToDatabase.Controllers
{
    public class HomeController : Controller
    {
        // URI base de donde se consumen los servicios
        string bseUri = "http://localhost:63351/api/";
        // Declaracion del contexto de base de datos
        private DbApplicationContext _db = new DbApplicationContext();
        // Para el seguimiento de progreso de carga 
        private static int progress;


        public ActionResult Index()
        {
            return View();
        }

        
        //public ActionResult SaveWebApiData()
        //{
        //    // Punto final a consumir
        //    string path = "data/entrada";
            
        //    var entradas = JsonConvert.DeserializeObject<List<EntradaViewModel>>(GetRequest(bseUri+path, HttpMethod.Get, null));

        //    foreach(var entrada in entradas)
        //    {
        //        _db.Entrada.Add(entrada);
        //    }
        //    _db.SaveChanges();
        //    _db.Dispose();


        //    return View("OperacionCompletada");
        //}


        [HttpPost]
        public JsonResult Start()
        {
            // Punto final a consumir
            string path = "data/entrada";



            Stopwatch timeMeasure = new Stopwatch();
            //timeMeasure.Start();
            //timeMeasure.Stop();
            //Console.WriteLine($"Tiempo: {timeMeasure.Elapsed.TotalMilliseconds} ms");

            timeMeasure.Start();
            
            var entradas = JsonConvert.DeserializeObject<List<EntradaViewModel>>(GetRequest(bseUri + path, HttpMethod.Get, null));

            int count = entradas.Count();
            int i = 0;

            foreach (var entrada in entradas)
            {
                _db.Entrada.Add(entrada);
                progress = Convert.ToInt32(Convert.ToDouble(i) / Convert.ToDouble(count) * 100);
                i = i+1 ;
            }
            //throw new Exception("error con la base de datos");
            _db.SaveChanges();
            _db.Dispose();

            timeMeasure.Stop();

            var result = new { StartResult = "ok", ElementsMigrated = count, ElapsedTime = $"Tiempo: {(timeMeasure.Elapsed.TotalMilliseconds / 1000) /60 }  minutos" };
            progress = 0;


            return Json(result);
        }


        [HttpPost]
        public JsonResult StartMassiveInjection()
        {

            Stopwatch timeMeasure = new Stopwatch();
            
            timeMeasure.Start();
            // Punto final a consumir
            string path = "data/entrada";

            List<EntradaViewModel> entradas = new List<EntradaViewModel>();

            // Simulando  entrada masiva de datos
            for (int j = 0; j<=1670; j++)
            {
                entradas.AddRange(JsonConvert.DeserializeObject<List<EntradaViewModel>>(GetRequest(bseUri + path, HttpMethod.Get, null)));
            }

            int count = entradas.Count();
            int i = 0;

            foreach (var entrada in entradas)
            {
                _db.Entrada.Add(entrada);
                progress = Convert.ToInt32(Convert.ToDouble(i) / Convert.ToDouble(count) * 100);
                //_db.SaveChanges();

                i = i + 1;
            }
            //throw new Exception("error con la base de datos");
            _db.SaveChanges();
            _db.Dispose();

            timeMeasure.Stop();

            //var result = new { StartResult = "ok", ElementsMigrated = count, ElapsedTime = $"Tiempo: {timeMeasure.Elapsed.TotalMilliseconds / 1000 } segundos" };
            var result = new { StartResult = "ok", ElementsMigrated = count, ElapsedTime = $"Tiempo: {decimal.Round(Convert.ToDecimal(((timeMeasure.Elapsed.TotalMilliseconds / 1000) / (60))),1) }  minutos" };

            progress = 0;

            return Json(result);
        }


        [HttpPost]
        public JsonResult Progress()
        {
            var result = new { ProgressResult = progress };
            return Json(result);
        }

       

        #region call webApi - helpers
        /// <summary>
        /// Metodo para realizar peticiones a un recurso web
        /// </summary>
        /// <param name="url"> url del punto final a llamar</param>
        /// <param name="httpMethod">Tipo de metodo http deseado (Get, Post, Put, Delete)</param>
        /// <param name="body">Cuerpo de la petición</param>
        /// <returns></returns>
        private string GetRequest(string url, HttpMethod httpMethod, string body = null)
        {
            try
            {
                var webRequest = WebRequest.Create(url);
                webRequest.Timeout = Timeout.Infinite;
                webRequest.Method = ToHttpMethodString(httpMethod);
                webRequest.ContentType = "application/json; charset=utf-8";
                if (body != null)
                {
                    using (var streamWriter = new StreamWriter(webRequest.GetRequestStream()))
                    {
                        streamWriter.Write(body);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                }

                var getResponse = (HttpWebResponse)webRequest.GetResponse();

                var newStream = getResponse.GetResponseStream();
                if (newStream == null) throw new Exception("La consulta externa no regreso nada");
                var sr = new StreamReader(newStream);
                var resultStringRequest = sr.ReadToEnd();
                return resultStringRequest;
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            var error = reader.ReadToEnd();
                            var result = JsonConvert.DeserializeObject<ErrorApiRequest>(error);
                            var exception = new Exception();
                           
                            throw exception;
                        }
                    }
                }
            }
            throw new Exception();
        }

        /// <summary>
        /// Clase poara manejar errores de la peticion api
        /// </summary>
        public class ErrorApiRequest
        {
            public string Message { get; set; }
            public Dictionary<string, string[]> ModelState { get; set; }
        }
        /// <summary>
        /// Recupera el metodo http a emplear
        /// </summary>
        /// <param name="httpMethod"></param>
        /// <returns></returns>
        private string ToHttpMethodString(HttpMethod httpMethod)
        {
            var result = "";
            switch (httpMethod)
            {
                case HttpMethod.Get:
                    result = "GET";
                    break;
                case HttpMethod.Post:
                    result = "POST";
                    break;
                case HttpMethod.Put:
                    result = "PUT";
                    break;
                case HttpMethod.Delete:
                    result = "DELETE";
                    break;
            }
            return result;
        }

        /// <summary>
        /// Enum Tipo de metodos http disponibles
        /// </summary>
        private enum HttpMethod
        {
            Get = 1,
            Post = 2,
            Put = 3,
            Delete = 4
        }
        #endregion 

    }
}