namespace WebApiToDatabase.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Entrada")]
    public partial class Entrada
    {
        [Key]
        public int PK_IdEntrada { get; set; }

        [StringLength(250)]
        public string Libro { get; set; }

        public int? AssetId { get; set; }

        [StringLength(250)]
        public string FechaEjecucion { get; set; }

        [StringLength(100)]
        public string Usuario { get; set; }

        public DateTime? PeriodoInicial { get; set; }

        [StringLength(50)]
        public string Area { get; set; }

        [StringLength(250)]
        public string NombreArea { get; set; }

        [StringLength(100)]
        public string Ubicacion { get; set; }

        [StringLength(100)]
        public string CveCabms { get; set; }

        [StringLength(250)]
        public string DescItem { get; set; }

        public int? FolioCc { get; set; }

        [StringLength(50)]
        public string NumActivo { get; set; }

        [StringLength(500)]
        public string DescActivo { get; set; }

        [StringLength(10)]
        public string Tipo { get; set; }

        [StringLength(50)]
        public string Marca { get; set; }

        [StringLength(250)]
        public string ModelNumber { get; set; }

        [StringLength(200)]
        public string SerialNumber { get; set; }

        public DateTime? FechaServicio { get; set; }

        [StringLength(50)]
        public string NumFactura { get; set; }

        [StringLength(50)]
        public string FechaFactura { get; set; }

        [StringLength(50)]
        public string NumRemision { get; set; }

        [StringLength(50)]
        public string FechaRemision { get; set; }

        [StringLength(50)]
        public string Folio { get; set; }

        public DateTime? TransactionDateEntered { get; set; }

        public double? ImporteActivo { get; set; }

        [StringLength(50)]
        public string Annio { get; set; }
    }
}
