namespace WebApiToDatabase.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DbApplicationContext : DbContext
    {
        public DbApplicationContext()
            : base("name=DbApplicationContext")
        {
        }

        public virtual DbSet<Entrada> Entrada { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
