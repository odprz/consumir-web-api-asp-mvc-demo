﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiToDatabase.Data
{
    /// <summary>
    /// ViewModel del resultado del servicio
    /// </summary>
    public class EntradaViewModel
    {
        /// <summary>
        /// Convierte viewmodel a una entidad para poder guardar en base de datos
        /// </summary>
        /// <param name="model"></param>
        public static implicit operator Entrada(EntradaViewModel model) {
            return new Entrada {
                Libro = model.libro,
                AssetId = model.asset_id,
                FechaEjecucion = model.fecha_ejecucion,
                Usuario = model.usuario,
                PeriodoInicial = model.periodo_inicial,
                Area = model.area,
                NombreArea = model.nombre_area,
                Ubicacion = model.ubicacion,
                CveCabms = model.cve_cabms,
                DescItem = model.desc_item,
                FolioCc = model.folio_cc,
                NumActivo = model.num_activo,
                DescActivo = model.desc_activo,
                Tipo = model.tipo,
                Marca = model.marca,
                ModelNumber = model.model_number,
                SerialNumber = model.serial_number,
                FechaServicio = model.fecha_servicio,
                NumFactura = model.num_factura,
                FechaFactura = model.fecha_factura,
                NumRemision = model.num_remision,
                FechaRemision = model.fecha_remision,
                Folio = model.folio,
                TransactionDateEntered = model.transaction_date_entered,
                ImporteActivo = model.importe_activo,
                Annio = model.annio
            }; 
        }

        #region attr
        [StringLength(250)]
        public string libro { get; set; }

        public int? asset_id { get; set; }

        [StringLength(250)]
        public string fecha_ejecucion { get; set; }

        [StringLength(100)]
        public string usuario { get; set; }

        public DateTime? periodo_inicial { get; set; }

        [StringLength(50)]
        public string area { get; set; }

        [StringLength(250)]
        public string nombre_area { get; set; }

        [StringLength(100)]
        public string ubicacion { get; set; }

        [StringLength(100)]
        public string cve_cabms { get; set; }

        [StringLength(250)]
        public string desc_item { get; set; }

        public int? folio_cc { get; set; }

        [StringLength(50)]
        public string num_activo { get; set; }

        [StringLength(500)]
        public string desc_activo { get; set; }

        [StringLength(10)]
        public string tipo { get; set; }

        [StringLength(50)]
        public string marca { get; set; }

        [StringLength(250)]
        public string model_number { get; set; }

        [StringLength(200)]
        public string serial_number { get; set; }

        public DateTime? fecha_servicio { get; set; }

        [StringLength(50)]
        public string num_factura { get; set; }

        [StringLength(50)]
        public string fecha_factura { get; set; }

        [StringLength(50)]
        public string num_remision { get; set; }

        [StringLength(50)]
        public string fecha_remision { get; set; }

        [StringLength(50)]
        public string folio { get; set; }

        public DateTime? transaction_date_entered { get; set; }

        public double? importe_activo { get; set; }

        [StringLength(50)]
        public string annio { get; set; }
        #endregion
    }
}
